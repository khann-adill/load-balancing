### Load Balancing using nginx reverse proxy
`Play with Load balancing`

### What is nginx reverse proxy.?
```
A reverse proxy server is a type of proxy server that typically sits behind the firewall in a private network and directs client requests to the appropriate backend server.

Common uses for a reverse proxy server include:

    1. Load balancing: A reverse proxy server can act as a “traffic cop,” sitting in front of your backend servers    and distributing client requests across a group of servers.

    2. Web acceleration: Reverse proxies can compress inbound and outbound data, as well as cache commonly requested content, both of which speed up the flow of traffic between clients and servers. 
    
    3. Security and anonymity: Ensures that multiple servers can be accessed from a single record locator or URL regardless of the structure of your local area network.
```
![alt text](<.images/nginx3.PNG>) 
### With BareMetal + docker

1. Two application running on docker app1 and app2
```
app1-> docker run -itd --name=nginx1 -p 8080:80 mak1806/custom-nginx:v1.0
app2-> docker run -itd --name=nginx2 -p 8081:80 mak1806/custom-nginx:v2.0
```
2a. Installing and configure nginx
```
-> apt install nginx -y
```
2b. Unlink the default site
```
 -> unlink /etc/nginx/sites-enabled/default
```
2c. Configuration
```
-> These two directries is mandatory 
        a. sites-available
        b. sites-enabled
-> cd /etc/nginx/sites-available
```
3. Creating reverse proxy config file
```sh
-> vim reverse-proxy.conf

upstream backend {
    server 127.0.0.1:8080;
    server 127.0.0.1:8081;
}
server {
     listen 80;
     location / {
                proxy_pass http://backend;
     }
}
```
4. Linking 
```
-> ln -s /etc/nginx/sites-available/reverse-proxy.conf /etc/nginx/sites-enabled/reverse-proxy.conf
```
5. Testing the nginx config
```
-> service nginx configtest
```
6. Restart nginx service
```
-> service nginx restart
```

### With Docker
1. Docker compose up
```
-> docker-compose up -d
```
2. Checking status
```sh
-> docker-compose ps
Name              Command               State          Ports
--------------------------------------------------------------------
app1   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8080->80/tcp
app2   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8081->80/tcp
lb     /docker-entrypoint.sh ngin ...   Up      0.0.0.0:80->80/tcp
```

##### Verify the work
``` 
Grab the server public IP address.
```
`Hit the Ip in your browser first time you will get this v1`
![alt text](<.images/nginxv1.PNG>) 
`Hit refresh`
![alt text](<.images/nginxv2.PNG>)
